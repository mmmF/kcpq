En la presente carpeta se encuentran los archivos y librerias necesarias para ejecutar el algoritmo KCPQ.
Para poder compilar los archivos, es necesario  usar el sistema operativo Linux tener instalado el 
paquete de g++.

*********************************************************************************************************


Existen 2 archivos main para compilar y ejecutar el KCPQ:

1. kcpqmain.cpp: Este programa fue creado con el proposito de ejecutar la consulta KCPQ sobre dos sets de puntos. Se ingresa el valor de K, entregando los resultados por pantalla junto al tiempo de ejecucion en base a la CPU. Para compilar este archivo usar comando
"g++ -O3 -o nombreDeEjecutable kcpqmain.cpp".

2. kcpqtestmain.cpp: Este pragrama fue creado con el proposito de realizar test de rendimiento en base a los tiempos de ejecucion, realizando 50 consultas para un caso mostrando el promedio por pantalla. Para compilar este archivo usar comando 
"g++ -O3 -o nombreDeEjecutable kcpqmain.cpp".

*********************************************************************************************************

Consideraciones:

-Los datasets que reciben los codigos deben estar en formato kt.

-Para probar un dataset es necesario modificar el archivo de fuente con las rutas de los dataset
y recompilar.

-El principal objetivo es presentar el código KCPQ para el uso que el usuario estime conveniente, 
por lo que los main presentados son opcionales y para usos específicos.

**********************************************************************************************************

Existe un tercer archivo para compilar en el caso que se desea probar un nuevo dataset en formato txt (pares de puntos), con 
el nombre "generateKtFromPointList.cpp" el cual transforma los archivos en formato kt.

Para hacer uso de este archivo se deben realizar los siguientes pasos y ejecutar en la consola de comandos:

1.compilacion: "g++ -o nombreDelEjecutable generateKtFromPointList.cpp" .

2.Funcionamiento: El programa recibe la ruta del archivo en formato txt por consola. Por ejemplo: si tenemos un dataset
"data.txt", el comando de transformacion es "./nombreDelEjecutable data.txt" (en el caso de que data.txt este en la misma
carpeta que el ejecutable). El programa nos entrega un nuevo archivo llamado data.kt, el cual esta listo para ser 
probado.


