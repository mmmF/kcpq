#include "knnpq.hpp"
#include "kcpq.hpp"
#include "knnpq.cpp"
#include "kTree.h"
#include "basic.h"
#include "math.h"
#include <unistd.h>
#include <algorithm>
#include <ctime>

using namespace std;

struct kcpqElementocola
{
    knnpqElement puntosR;
    knnpqElement puntosS;
    double distance;      
    
    kcpqElementocola(){};
    ~kcpqElementocola(){};

    kcpqElementocola(knnpqElement R, knnpqElement S, double _distance)
    {
        puntosR = R;
        puntosS = S;
        distance = _distance;
    }        

};

bool compMinHeapK(kcpqElementocola A_element, kcpqElementocola B_element)
{
    double a = A_element.distance;
    double b = B_element.distance;
    return a > b;
};

bool compMaxHeapK(kcpqElementocola A_element, kcpqElementocola B_element)
{
    double a = A_element.distance;
    double b = B_element.distance;
    return a < b;
};

double kcpq::minMinDist(Quadrant R1, Quadrant R2)
{
    double y1 = Function(R1.getS().first ,R1.getT().first, R2.getS().first, R2.getT().first); 
    double y2 = Function(R1.getS().second, R1.getT().second, R2.getS().second, R2.getT().second);
    return sqrt((y1*y1 + y2*y2));
};

double kcpq::Function(float si, float ti, float pi, float qi)
{
    if (qi<si)
        return abs(si-qi);
    if (pi>ti)
        return abs(pi-ti); 
    return 0.0;         
};

vector<kcpqElementocola> kcpq::KCPQ(MREP *rep1, MREP *rep2, int k){

    vector<kcpqElementocola> pQueue, Cand;
    vector<kcpqElementocola> Puntos;
    
    Quadrant quadR1 = Quadrant(make_pair(1, 1), make_pair(rep1->numberOfNodes,rep1->numberOfNodes));
    Quadrant quadS1 = Quadrant(make_pair(1, 1), make_pair(rep2->numberOfNodes,rep2->numberOfNodes));

    double dist = minMinDist(quadR1, quadS1);

    knnpqElement R1 = knnpqElement(-1, quadR1, 0, 0);
    knnpqElement S1 = knnpqElement(-1, quadS1, 0, 0);

    kcpqElementocola e = kcpqElementocola(R1,S1, dist);

    make_heap(pQueue.begin(), pQueue.end(), compMinHeapK);
    make_heap(Cand.begin(), Cand.end(), compMaxHeapK);
    
    pQueue.push_back(e);

    while(pQueue.size() != 0)
    {
        e = pQueue.front();
        pop_heap(pQueue.begin(), pQueue.end(), compMinHeapK);
        pQueue.pop_back();  

        if(Cand.size() == k && e.distance >= Cand.front().distance)
        {
            for(kcpqElementocola aux : Cand){
                Puntos.push_back(aux);
            }
            
            return Puntos;
        }

        //bool isLeaf = false;

        if (e.puntosR.level > rep1->maxLevel && e.puntosS.level>rep2->maxLevel)
        { 
            //kcpqElementocola e2 = e;
            
            if(Cand.size()< k)
            {
                Cand.push_back(e);
                push_heap(Cand.begin(), Cand.end(), compMaxHeapK);
            }
            else{
            if(e.distance < Cand.front().distance){
                    pop_heap(Cand.begin(), Cand.end(), compMaxHeapK);
                    Cand.pop_back();
                    Cand.push_back(e);
                    push_heap(Cand.begin(), Cand.end(), compMaxHeapK);
            }  
            }
           
        }else if(e.puntosR.level > rep1->maxLevel){
            // comparar el punto que esta en R conlos cuadrantes de S y meterlos a la Pqueue;
            vector<knnpqElement> hijosDeS = getChildrenOfElement(e.puntosS, rep2->maxLevel, rep2->btl);

            for(knnpqElement aucs2 : hijosDeS){
                
                double minmindist = minMinDist(e.puntosR.quadrant, aucs2.quadrant);

                if((Cand.size() < k || minmindist< Cand.front().distance))
                    {
                        kcpqElementocola coso = kcpqElementocola(e.puntosR, aucs2, minmindist);
                        pQueue.push_back(coso);
                        push_heap(pQueue.begin(), pQueue.end(), compMinHeapK);
                          
                    }
            
             }                
                  
        }else if(e.puntosS.level>rep2->maxLevel){
            //puntos de s con los hijos de R y se inserta a Pqueue;
            vector<knnpqElement> hijosDeR = getChildrenOfElement(e.puntosR, rep1->maxLevel, rep1->btl);

            for(knnpqElement aucs1 : hijosDeR){
                    
                double minmindist = minMinDist(aucs1.quadrant, e.puntosS.quadrant);
          
                if((Cand.size() < k || minmindist< Cand.front().distance))
                {
                    kcpqElementocola coso = kcpqElementocola(aucs1, e.puntosS, minmindist);      
                    pQueue.push_back(coso);
                    push_heap(pQueue.begin(), pQueue.end(), compMinHeapK);
                            
                }
            
            }               
        }   
        else
        {
             
                vector<knnpqElement> hijosDeR = getChildrenOfElement(e.puntosR, rep1->maxLevel, rep1->btl); // Function that returns vector of childrens of element
                vector<knnpqElement> hijosDeS = getChildrenOfElement(e.puntosS, rep2->maxLevel, rep2->btl); // Function that returns vector of childrens of element
                
                    
                for(knnpqElement aucs1 : hijosDeR){
                    for(knnpqElement aucs2 : hijosDeS){

                        double minmindist = minMinDist(aucs1.quadrant, aucs2.quadrant);

                        if((Cand.size() < k || minmindist< Cand.front().distance))
                        {
                            kcpqElementocola coso = kcpqElementocola(aucs1, aucs2, minmindist);
                            pQueue.push_back(coso);
                            push_heap(pQueue.begin(), pQueue.end(), compMinHeapK);
                            
                        }
            
                    }                
                }   
            
        }
    }

    for(kcpqElementocola aux : Cand){
        Puntos.push_back(aux);
    }
       
    return Puntos;
}

vector<knnpqElement> kcpq::getChildrenOfElement(knnpqElement element, uint max_level, bitRankW32Int *btl)
{

    int quad_x = 0;
    int quad_y = 0;

    pair<int, int> S = element.quadrant.getS();
    pair<int, int> T = element.quadrant.getT();

    vector<knnpqElement> childrens;

    uint childrenPosition = element.position == -1 ? 0 : rank1(btl, element.position) * pow(K, 2);
    uint nChildren = K * K;

    int secuence = (element.quadrant.getT().second - element.quadrant.getS().second) / K;

    Quadrant temp_quadrant;

    quad_x = S.first;
    quad_y = S.second;

    for (int i = 0; i < nChildren; i++)
    {


        if (isBitSet(btl, childrenPosition))
        {

            temp_quadrant = Quadrant(make_pair(quad_x, quad_y), make_pair(quad_x + secuence, quad_y + secuence));
            int64_t minD = 0;
            knnpqElement new_element = knnpqElement(childrenPosition, temp_quadrant, minD, element.level + 1);
            childrens.push_back(new_element);
        }

        quad_x = quad_x + secuence;

        if( (i+1)% K == 0){
            quad_x = S.first;
            quad_y = quad_y + secuence;
        }
        
        childrenPosition++;
    }

    return childrens;
};
