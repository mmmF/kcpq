#ifndef KCPQ_H
#define KCPQ_H

#include <utility>
#include <queue>
#include <math.h>
#include "iostream"

#include "kTree.h"
#define K 2

using namespace std;

struct knnpqElement;
class Quadrant;
struct kcpqElementocola;
struct puntoskcpq;

class kcpq
{
public:

	kcpq() {}
	~kcpq() {}
	double minMinDist(Quadrant A_quadrant, Quadrant B_quadrant);
	vector<knnpqElement> getChildrenOfElement(knnpqElement element, uint max_level, bitRankW32Int *btl); // Returns the K^2 childrens of element
	//bool esCandidato( double minminDist, vector<kcpqElementocola> pQueue, vector<kcpqElementocola> Cand, int k);
	vector<kcpqElementocola> KCPQ(MREP *rep1, MREP *rep2, int test);
	vector<pair<int,int>> kcpqbase(MREP * rep1, MREP rep2, int k);


private:

	double Function(float si, float ti, float pi, float qi);
};



#endif