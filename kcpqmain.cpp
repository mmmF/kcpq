#include "knnpq.hpp"
#include "kcpq.hpp"
#include "kcpq.cpp"
#include "kTree.h"
#include "basic.h"
#include "math.h"
#include <unistd.h>
#include <algorithm>
#include <ctime>

int main()
{
    char* name ="Datasets/Gauss/test4gf";
    char* name2 ="Datasets/Gauss/test44gf";

    int k;

    /*char name3[6];
    char name4[6];

    cout<< "ingrese el primer arbol(R): ";
    cin>> name3;
    cout<< "ingrese el segundo arbol(S): ";
    cin>> name4;

    char* name = name3;

    char* name2 = name4;*/

    MREP * arbol = loadRepresentation(name);
    MREP * arbol2 = loadRepresentation(name2);

    kcpq prueba;
    
    cout << "Escribe valor de k: ";
    cin >> k;

    clock_t c_start = std::clock();
    vector <kcpqElementocola> vkcpq = prueba.KCPQ(arbol,arbol2,k);
    clock_t c_end = std::clock();

    double tiempo = 1000000000 * (c_end-c_start) / CLOCKS_PER_SEC;
    sort(vkcpq.begin(), vkcpq.end(), compMaxHeapK);

    for(auto it = vkcpq.begin(); it != vkcpq.end(); it++)
    {
        cout << it->puntosR.quadrant.getS().first << "," << it->puntosR.quadrant.getS().second << " - ";
        cout << it->puntosS.quadrant.getS().first << "," << it->puntosS.quadrant.getS().second << " con distancia: ";
        cout << it->distance << endl;
    }

    cout<<"uso de CPU: " << tiempo<< "s" << endl;
    
    cout<< "******TERMINO******";
}